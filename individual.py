import pandas as pd
import file_reader as fr
import functions as fn
import os


correct = fr.correct
df_answers = fr.df_answers
df_student = fr.df_student
ng = fr.noneng

#Output folder and file name
output_foldername = fn.translation("Output/Output for Customers",ng)
output_filename = fn.translation('Individual Result for each Institute.xlsx',ng)
if not os.path.exists(output_foldername):
    os.makedirs(output_foldername)
output_filepath = output_foldername + '/' + output_filename
writer = pd.ExcelWriter(output_filepath)

cdf = (correct.values == df_answers)

ind = df_student[['Examinee No.','Name','Year','Institute']].copy()
ind['Total'] = cdf.sum(axis=1)
ind['Ranking in Whole Year'] = ind['Total'].rank(method = 'min', ascending = False).astype('int')
ind['Ranking in Same Year'] = ind[['Year','Total']].groupby('Year').rank(method = 'min', ascending = False)
new = ind[0:1]
ind = pd.concat([ind, cdf.sum(axis=1,level=0).astype('int')], axis=1)


# New row with full marks to concate at the top
text = fn.translation('Full Marks',ng)
new.loc[:] = '-'
totalsub = 0
for i in range(0,fr.nsub):
    subn = fr.subject[i].name
    subm = fr.subject[i].tqno
    totalsub += fr.subject[i].tqno
    new[subn] = subm
new.loc[0,'Total'] = totalsub
new.iloc[0,0] = text

avg = fn.translation('Average for Institute(Except Absentee)',ng)
listins = ind.Institute.unique()
workbook  = writer.book
for ins in listins:
    output_sheet_name = ins
    row = 10
    col = 1
    individual = ind.loc[ind['Institute'] == ins]
    individual = individual.sort_values('Year',ascending=True)
    individual = individual.copy()
    individual['Total'] = individual.loc[:,'Total'].astype(float)
    individual.loc['Average','Total'] = round(individual['Total'].mean(),2)
    indmean = round(individual.iloc[:,7:].mean(),2)
    individual.iloc[-1,7:] = indmean
    individual.loc['Average','Examinee No.'] = avg
    individual = pd.concat([new,individual])
    individual.drop('Institute',axis=1)
    individual.set_index('Examinee No.')
    fn.formatter(individual, writer, output_sheet_name , row , col , index=False , noneng = ng)
    row = 1
    col = 1
    worksheet = writer.sheets[output_sheet_name]
    text = fn.translation('Result Table for Institute',ng)
    options = {
        'width': 1500,
        'height': 50,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 20},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#e6e6e6'},
        'border': {'color': 'black','width': 2},
    }
    worksheet.insert_textbox(row, col, text, options)
    row += 4
    col = 16
    text = fn.translation('Institute',ng)
    options = {
        'width': 100,
        'height': 40,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 16},
        'fill': {'color': '#e6e6e6'},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)
    text = ins
    options = {
        'width': 300,
        'height': 40,
        'x_offset': 100,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 16},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)
    row += 2
    col = 1
    text = fn.translation('Individual Result for each Institute(Order by Year, Name )',ng)
    text = '▣ ' + text
    options = {
        'height': 40,
        'width' : 700,
        'font': {'bold': True,'name': 'Arial','size': 16},
    }
    worksheet.insert_textbox(row, col, text, options)
    
if ng:
    fn.cprint(output_filename + " 결과 파일 출력됨", "green", "black")
else:
    fn.cprint("Output file " + output_filename + " Created", "green", "black")
writer.save()