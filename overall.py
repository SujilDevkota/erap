import pandas as pd
import numpy as np
import file_reader as fr
import functions as fn
import os


correct = fr.correct
df_answers = fr.df_answers
df_student = fr.df_student
ng = fr.noneng
nsub = fr.nsub
subject = fr.subject

#Output folder and file name
output_foldername = fn.translation("Output/Output for Customers",ng)
output_filename = fn.translation('Overall Result for a Society.xlsx',ng)
if not os.path.exists(output_foldername):
    os.makedirs(output_foldername)
output_filepath = output_foldername + '/' + output_filename
writer = pd.ExcelWriter(output_filepath)
workbook  = writer.book

# Calculate Average Level and discrimination rate
# Comparing student answers with correct answers
cdf = (correct.values == df_answers)
cdf.insert(0, 'Total Score', cdf.sum(axis=1), allow_duplicates=False)
cdf = cdf[cdf['Total Score'] != 0]
cdf.insert(1, 'Standing', cdf['Total Score'].rank(method = 'min', ascending = False), allow_duplicates=False)
cdf.insert(2, 'Percentage', round(cdf['Standing']/len(cdf),4), allow_duplicates=False)
cdf = cdf[cdf['Total Score'] != 0]

dislem = correct.T
dislem['Winners in Top 27%'] = ((cdf['Percentage'] <= .27) & cdf.iloc[:,3:].T).sum(axis=1)
dislem['Examinees in Top 27%'] = (cdf['Percentage'] <= .27).sum()
dislem['Winners in Low 27%'] = ((cdf['Percentage'] >= .73) & cdf.iloc[:,3:].T).sum(axis=1)
dislem['Examinees in Low 27%'] = (cdf['Percentage'] >= .73).sum()
temp = dislem['Winners in Top 27%']/dislem['Examinees in Top 27%'] -dislem['Winners in Low 27%']/dislem['Examinees in Low 27%']


output_sheet_name = 'Score Distribution Table'
row = 1
col = 1
worksheet = workbook.add_worksheet(output_sheet_name)
text = fn.translation('UBT SCORE DISTRIBUTION TABLE',ng)
text = '2018' + text
options = {
    'width': 1344,
    'height': 60,
    'font': {'bold': True,'name': 'Arial','align': 'center','size': 20},
    'align': {'horizontal': 'center','vertical': 'middle'},
    'fill': {'color': '#e6e6e6'},
    'border': {'color': 'black','width': 2},
}
worksheet.insert_textbox(row, col, text, options)
row += 6
col = 3

# Header of the table
text = ''
options = {
    'width': 256,
    'height': 40,
    'fill': {'color': '#e6e6e6'},
    'border': {'color': 'black','width': 1},
}
worksheet.insert_textbox(row, col, text, options)
text = fn.translation('Range/Average',ng)
options = {
    'width': 192,
    'height': 80,
    'x_offset': 256,
    'font': {'bold': True,'name': 'Arial','align': 'center','size': 14},
    'align': {'horizontal': 'center','vertical': 'middle'},
    'fill': {'color': '#e6e6e6'},
    'border': {'color': 'black','width': 1},
}
worksheet.insert_textbox(row, col, text, options)
text = fn.translation('Score Distribution',ng)
options = {
    'width': 640,
    'height': 40,
    'x_offset': 448,
    'font': {'bold': True,'name': 'Arial','align': 'center','size': 14},
    'align': {'horizontal': 'center','vertical': 'middle'},
    'fill': {'color': '#e6e6e6'},
    'border': {'color': 'black','width': 1},
}
worksheet.insert_textbox(row, col, text, options)
text = fn.translation('Subjects',ng)
options = {
    'width': 64,
    'height': 40,
    'y_offset': 40,
    'font': {'bold': True,'name': 'Arial','align': 'center','size': 10},
    'align': {'horizontal': 'center','vertical': 'middle'},
    'fill': {'color': '#e6e6e6'},
    'border': {'color': 'black','width': 1},
}
worksheet.insert_textbox(row, col, text, options)
text = fn.translation('Full Marks & Analysis Result',ng)
options = {
    'width': 192,
    'height': 40,
    'x_offset': 64,
    'y_offset': 40,
    'font': {'bold': True,'name': 'Arial','align': 'center','size': 12},
    'align': {'horizontal': 'center','vertical': 'middle'},
    'fill': {'color': '#e6e6e6'},
    'border': {'color': 'black','width': 1},
}
worksheet.insert_textbox(row, col, text, options)
text = fn.translation('1st Year',ng)
options = {
    'width': 128,
    'height': 40,
    'x_offset': 448,
    'y_offset': 40,
    'font': {'bold': True,'name': 'Arial','align': 'center','size': 14},
    'align': {'horizontal': 'center','vertical': 'middle'},
    'fill': {'color': '#e6e6e6'},
    'border': {'color': 'black','width': 1},
}
worksheet.insert_textbox(row, col, text, options)
text = fn.translation('2nd Year',ng)
options = {
    'width': 128,
    'height': 40,
    'x_offset': 576,
    'y_offset': 40,
    'font': {'bold': True,'name': 'Arial','align': 'center','size': 14},
    'align': {'horizontal': 'center','vertical': 'middle'},
    'fill': {'color': '#e6e6e6'},
    'border': {'color': 'black','width': 1},
}
worksheet.insert_textbox(row, col, text, options)
text = fn.translation('3rd Year',ng)
options = {
    'width': 128,
    'height': 40,
    'x_offset': 704,
    'y_offset': 40,
    'font': {'bold': True,'name': 'Arial','align': 'center','size': 14},
    'align': {'horizontal': 'center','vertical': 'middle'},
    'fill': {'color': '#e6e6e6'},
    'border': {'color': 'black','width': 1},
}
worksheet.insert_textbox(row, col, text, options)
text = fn.translation('4th Year',ng)
options = {
    'width': 128,
    'height': 40,
    'x_offset': 832,
    'y_offset': 40,
    'font': {'bold': True,'name': 'Arial','align': 'center','size': 14},
    'align': {'horizontal': 'center','vertical': 'middle'},
    'fill': {'color': '#e6e6e6'},
    'border': {'color': 'black','width': 1},
}
worksheet.insert_textbox(row, col, text, options)
text = fn.translation('Total',ng)
options = {
    'width': 128,
    'height': 40,
    'x_offset': 960,
    'y_offset': 40,
    'font': {'bold': True,'name': 'Arial','align': 'center','size': 14},
    'align': {'horizontal': 'center','vertical': 'middle'},
    'fill': {'color': '#e6e6e6'},
    'border': {'color': 'black','width': 1},
}
worksheet.insert_textbox(row, col, text, options)



# Table data start from here
row += 4
for i in range(0,nsub):
    sub_name = subject[i].name
    lm = round((cdf[sub_name].sum(axis=1)/len(cdf)).sum()/subject[0].tqno,4)
    dm = round(temp[sub_name].sum()/len(temp[sub_name]),4)
    text = sub_name
    options = {
        'width': 64,
        'height': 99,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 10},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#e6e6e6'},
        
    }
    worksheet.insert_textbox(row, col, text, options)
    text = fn.translation('Full Marks',ng)
    options = {
        'width': 128,
        'height': 33,
        'x_offset' : 64,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 10},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#f5f5f5'},
        
    }
    worksheet.insert_textbox(row, col, text, options)
    text = fn.translation('Level Mean',ng)
    options = {
        'width': 128,
        'height': 33,
        'x_offset' : 64,
        'y_offset' : 33,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 10},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#f5f5f5'},
        
    }
    worksheet.insert_textbox(row, col, text, options)
    text = fn.translation('Discrimination Mean',ng)
    options = {
        'width': 128,
        'height': 33,
        'x_offset' : 64,
        'y_offset' : 66,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 10},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#f5f5f5'},
        
    }
    worksheet.insert_textbox(row, col, text, options)
    text = str(subject[i].tqno)
    options = {
        'width': 64,
        'height': 33,
        'x_offset' : 192,
        'font': {'name': 'Arial','align': 'center','size': 10},
        'align': {'horizontal': 'center','vertical': 'middle'},
        
    }
    worksheet.insert_textbox(row, col, text, options)
    text = str(lm)
    options = {
        'width': 64,
        'height': 33,
        'x_offset' : 192,
        'y_offset' : 33,
        'font': {'name': 'Arial','align': 'center','size': 10},
        'align': {'horizontal': 'center','vertical': 'middle'},
        
    }
    worksheet.insert_textbox(row, col, text, options)
    text = str(dm)
    options = {
        'width': 64,
        'height': 33,
        'x_offset' : 192,
        'y_offset' : 66,
        'font': {'name': 'Arial','align': 'center','size': 10},
        'align': {'horizontal': 'center','vertical': 'middle'},
        
    }
    worksheet.insert_textbox(row, col, text, options)
    text = fn.translation('Range',ng)
    options = {
        'width': 192,
        'height': 33,
        'x_offset' : 256,
        'font': {'name': 'Arial','align': 'center','size': 12},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#f5f5f5'},
        
    }
    worksheet.insert_textbox(row, col, text, options)
    text = fn.translation('Average',ng)
    options = {
        'width': 64,
        'height': 66,
        'x_offset' : 256,
        'y_offset' : 33,
        'font': {'name': 'Arial','align': 'center','size': 10},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#f5f5f5'},
        
    }
    worksheet.insert_textbox(row, col, text, options)
    text = fn.translation('Original Score',ng)
    options = {
        'width': 128,
        'height': 33,
        'x_offset' : 320,
        'y_offset' : 33,
        'font': {'name': 'Arial','align': 'center','size': 10},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#f5f5f5'},
    }
    worksheet.insert_textbox(row, col, text, options)
    text = fn.translation('Converted Score(100)',ng)
    options = {
        'width': 128,
        'height': 33,
        'x_offset' : 320,
        'y_offset' : 66,
        'font': {'name': 'Arial','align': 'center','size': 10},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#f5f5f5'},
    }
    worksheet.insert_textbox(row, col, text, options)


    # Comparing student answers with correct answers
    cdf = (correct.values == df_answers)
    cf = cdf.copy()
    cf.columns = cf.columns.droplevel([1,2,3])
    del cf.columns.name
    ts = df_student[['Year']].copy()
    ts = ts.join(cf)
    # year = ts['Year'].unique()
    # year = np.sort(year)
    
    # Considering the year value only ranges from(1 to 4) and 5th loop for Total
    for j in range(1,6):
        if j<=4:
            score = ts.loc[ts['Year']==j][subject[i].name].sum(axis=1).mean()
            mini = ts.loc[ts['Year']==j][subject[i].name].sum(axis=1).min()
            maxi = ts.loc[ts['Year']==j][subject[i].name].sum(axis=1).max()
            conv = (score/subject[i].tqno) * 100
        else:
            score = ts[subject[i].name].sum(axis=1).mean()
            mini = ts[subject[i].name].sum(axis=1).min()
            maxi = ts[subject[i].name].sum(axis=1).max()
            conv = (score/subject[i].tqno) * 100

        text = str(mini) + '  ~  ' + str(maxi)
        options = {
            'width': 128,
            'height': 33,
            'x_offset' : 448 + (j-1)*128,
            'font': {'name': 'Arial','align': 'center','size': 10},
            'align': {'horizontal': 'center','vertical': 'middle'},
        }
        worksheet.insert_textbox(row, col, text, options)

        text = str(round(score,2))
        options = {
            'width': 128,
            'height': 33,
            'x_offset' : 448 + (j-1)*128,
            'y_offset' : 33,
            'font': {'name': 'Arial','align': 'center','size': 10},
            'align': {'horizontal': 'center','vertical': 'middle'},
        }
        worksheet.insert_textbox(row, col, text, options)

        text = str(round(conv,2))
        options = {
            'width': 128,
            'height': 33,
            'x_offset' : 448 + (j-1)*128,
            'y_offset' : 66,
            'font': {'name': 'Arial','align': 'center','size': 10},
            'align': {'horizontal': 'center','vertical': 'middle'},
        }
        worksheet.insert_textbox(row, col, text, options)


    row += 5

if ng:
    fn.cprint(output_filename + " 결과 파일 출력됨", "green", "black")
else:
    fn.cprint("Output file " + output_filename + " Created", "green", "black")

writer.save()