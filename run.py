import time
import shutil

start = time.process_time()
print("====================================================================")
import exam_analysis
print("====================================================================")
print("====================================================================")
import individual
print("--------------------------------------------------------------------")
import personal
print("--------------------------------------------------------------------")
import overall
print("====================================================================")
stop = time.process_time()
elasped = stop-start
print("Total Time: " + str(elasped) + " sec")
shutil.rmtree('__pycache__')
