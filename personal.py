import pandas as pd
import file_reader as fr
import functions as fn
import os

correct = fr.correct
df_answers = fr.df_answers
df_student = fr.df_student
subject = fr.subject
nsub = fr.nsub
ng = fr.noneng

# Output folder and file name
output_foldername = fn.translation("Output/Output for Customers",ng)
output_filename = fn.translation('Personal Result for each person.xlsx',ng)
if not os.path.exists(output_foldername):
    os.makedirs(output_foldername)
output_filepath = output_foldername + '/' + output_filename
writer = pd.ExcelWriter(output_filepath)
workbook  = writer.book

# Generates dataframe that checks individual answers to correct answers
cdf = (correct.values == df_answers)

# Dataframe that has subject-wise score and ranking of each student
sdf = df_student.iloc[:,1:5].copy()
for i in range(0,nsub):
    subname = subject[i].name
    sdf[subname,'Score'] = cdf[subname].sum(axis=1)
    sdf[subname,'Rank in total'] = sdf[subname,'Score'].rank(method = 'min', ascending = False)
    sdf[subname,'Rank in Year'] = sdf.filter([(subname,'Score'),'Year']).groupby(['Year']).rank(method = 'min', ascending=False)
sdf['Total','Score'] = cdf.sum(axis=1)
sdf['Total','Rank in total'] = sdf['Total','Score'].rank(method = 'min', ascending = False)
sdf['Total','Rank in Year'] = sdf.filter([('Total','Score'),'Year']).groupby(['Year']).rank(method = 'min', ascending=False)
tsdf = sdf.T

chart_values = sdf.loc[:,[('Total','Score')]].apply(pd.value_counts).fillna(0)
chart_values = chart_values.sort_index()
chart_len = len(chart_values)
chart_sheetname = "Sheet00"
chart_values.to_excel(writer,sheet_name = chart_sheetname, startrow = 0, startcol = 0)
chart_year = sdf['Year'].unique()
chart_len1 = {}
for i in chart_year:
    chart_values1  = sdf.loc[sdf['Year'] == i].loc[:,[('Total','Score')]].apply(pd.value_counts).fillna(0)
    chart_values1 = chart_values1.sort_index()
    chart_len1[i] = len(chart_values1)
    chart_values1.index.name = 'year' + str(i)
    chart_values1.to_excel(writer,sheet_name = chart_sheetname, startrow = 0, startcol = i*2)

# Dataframe for displaying number of students 
pre_student = fn.present_students(df_student)
tab1 = len(pre_student.loc[pre_student['Year']==1])
tab2 = len(pre_student.loc[pre_student['Year']==2])
tab3 = len(pre_student.loc[pre_student['Year']==3])
tab4 = len(pre_student.loc[pre_student['Year']==4])
tabt = len(pre_student)
tab = pd.DataFrame(data={'1st Year':[tab1],'2nd Year':[tab2],'3rd Year':[tab3],'4th Year':[tab4],'Total':[tabt]})

total_score=0
for i in range(0,nsub):
    total_score += subject[i].tqno

# loop for each individual student
for j in tsdf:
    output_sheet_name = 'sheet' + str(j)
    # tsdf[j]['Name']
    row = 12
    col = 1
    per = pd.DataFrame('',  columns=['Subjects','Full Marks(Converted F.M.)','Score(Converted Score)',
                                 'Standing in Your Year','Standing in Total'], index=range(0,nsub))
    # Generates each individual student result subject-wise
    for i in range(0,nsub):
        subname = subject[i].name
        per['Subjects'][i] = subname
        per['Full Marks(Converted F.M.)'][i] = str(subject[i].tqno) + "  (100)"
        score = tsdf[j][(subname,'Score')]
        conscore = round((score/subject[i].tqno)*100,1)
        per['Score(Converted Score)'][i] = str(score) + "  (" + str(conscore) + ")"
        per['Standing in Your Year'][i] = tsdf[j][(subname,'Rank in Year')]
        per['Standing in Total'][i] = tsdf[j][(subname,'Rank in total')]
    per.loc['Total','Subjects'] = fn.translation('Total',ng)
    per.loc['Total','Full Marks(Converted F.M.)'] = str(total_score) + "  (100)"
    per.loc['Total','Score(Converted Score)'] = str(tsdf[j][('Total','Score')]) + "  (" + str(round((tsdf[j][('Total','Score')]/total_score)*100,1)) + ")"
    per.loc['Total','Standing in Your Year'] = tsdf[j][('Total','Rank in Year')]
    per.loc['Total','Standing in Total'] = tsdf[j][('Total','Rank in total')]
    # per = per.set_index('Subjects')
    fn.formatter(per, writer, output_sheet_name , row , col , index=False , noneng = ng)
    chart_row = len(per) + row + 3

    row = 1
    col = 1
    worksheet = writer.sheets[output_sheet_name]
    text = fn.translation('Personal Exam Result',ng)
    options = {
        'width': 960,
        'height': 50,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 20},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#e6e6e6'},
        'border': {'color': 'black','width': 2},
    }
    worksheet.insert_textbox(row, col, text, options)
    row += 4
    # First texbox row with institute and year info
    text = fn.translation('Institute',ng)
    options = {
        'width': 160,
        'height': 40,
        'align': {'horizontal': 'center'},
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 16},
        'fill': {'color': '#e6e6e6'},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)
    text = tsdf[j]['Institute']
    options = {
        'width': 320,
        'height': 40,
        'x_offset': 160,
        'align': {'horizontal': 'center'},
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 16},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)

    text = fn.translation('Year',ng)
    options = {
        'width': 160,
        'height': 40,
        'x_offset': 480,
        'align': {'horizontal': 'center'},
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 16},
        'fill': {'color': '#e6e6e6'},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)
    text = str(tsdf[j]['Year'])
    options = {
        'x_offset': 640,
        'width': 320,
        'height': 40,
        'align': {'horizontal': 'center'},
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 16},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)

    # Second texbox row with License No and Name info
    text = fn.translation('License No.',ng)
    options = {
        'width': 160,
        'height': 40,
        'y_offset' : 40,
        'align': {'horizontal': 'center'},
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 16},
        'fill': {'color': '#e6e6e6'},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)
    text = str(tsdf[j]['License No.'])
    options = {
        'width': 320,
        'height': 40,
        'x_offset': 160,
        'y_offset' : 40,
        'align': {'horizontal': 'center'},
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 16},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)

    text = fn.translation('Name',ng)
    options = {
        'width': 160,
        'height': 40,
        'x_offset': 480,
        'y_offset' : 40,
        'align': {'horizontal': 'center'},
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 16},
        'fill': {'color': '#e6e6e6'},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)
    text = tsdf[j]['Name']
    options = {
        'width': 320,
        'height': 40,
        'y_offset' : 40,
        'x_offset': 640,
        'align': {'horizontal': 'center'},
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 16},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)
    row = chart_row
    text = fn.translation('Number of Examinees',ng)
    text = '▣ ' + text
    options = {
        'height': 40,
        'width' : 200,
        'font': {'bold': True,'name': 'Arial','size': 14},
    }
    worksheet.insert_textbox(row, col, text, options)
    tab.to_excel(writer, sheet_name = output_sheet_name , startrow = row + 3 , startcol = col , header=False , index=False)
    header_format = workbook.add_format({
        'bold': True,
        'align': 'center',
        'fg_color': '#f2f2f2',
        'border': 1 })
    endcol = len(tab.columns)
    endrow = row + len(tab) + 2
    worksheet.add_table(row + 2, col, endrow, endcol, {'header_row': False,'style': 'Table Style Light 8'})
    for col_num, value in enumerate(tab.columns.values):
        coll = col + col_num
        value = fn.translation(value,ng)
        worksheet.write(row + 2 , coll , value , header_format)

    # Plotting clusterd column chart 
    worksheet.insert_textbox(row, col, text, options)
    row = chart_row + 5
    text = fn.translation('Result Distribution Graph',ng)
    text = '▣ ' + text
    options = {
        'height': 40,
        'width' : 200,
        'font': {'bold': True,'name': 'Arial','size': 14},
    }
    worksheet.insert_textbox(row, col, text, options)

    chart_row += 7
    chart1 = workbook.add_chart({'type': 'column'})
    chart1.add_series({
        'categories': [chart_sheetname,1,0,chart_len,0],
        'values':   [chart_sheetname,1,1,chart_len,1],
    })
    name = fn.translation('Total Result Distribution graph',ng)
    chart1.set_title ({'name': name, 'name_font': {'size': 12, 'bold': True}})
    chart1.set_legend({'none': True})
    chart1.set_x_axis({'name': fn.translation('Original Score',ng)})
    chart1.set_y_axis({'name': fn.translation('Person',ng)})
    worksheet.insert_chart(chart_row , col ,chart1)

    chart_year = tsdf[j]['Year']
    chart_col = chart_year * 2
    chart2 = workbook.add_chart({'type': 'column'})
    chart2.add_series({
        'categories': [chart_sheetname , 1 , chart_col , chart_len1[chart_year] , chart_col],
        'values':   [chart_sheetname , 1 , chart_col+1 , chart_len1[chart_year] , chart_col+1],
    })
    name = str(chart_year) + fn.translation('th year Result Distribution graph',ng)
    name = fn.translation(name,ng)
    chart2.set_title ({'name': name, 'name_font': {'size': 12, 'bold': True}})
    chart2.set_legend({'none': True})
    chart2.set_x_axis({'name': fn.translation('Original Score',ng)})
    chart2.set_y_axis({'name': fn.translation('Person',ng)})
    worksheet.insert_chart(chart_row , 6 ,chart2)
   
if ng:
    fn.cprint(output_filename + " 결과 파일 출력됨", "green", "black")
else:
    fn.cprint("Output file " + output_filename + " Created", "green", "black")

writer.save()