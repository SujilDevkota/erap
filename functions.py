
import pandas as pd
import numpy as np
from colorama import init, Fore, Back, Style
init()

# Reading translation as dictionary
df = pd.read_excel("translate.xlsx")
df = df.set_index('English')
trans = df.to_dict()
trans = trans['Korean']

# Function to count applicant yearwise
# Retrive two dataframe(dfa,dfb),column name(cn),year value(y)
def yearcount(dfa,dfb,cn,y):
    flag=0
    n=len(dfa.index)
    co= [0] * n
    for i in dfa.index:
        iname=dfa[cn][i]
        for j in dfb.index:
            jname=dfb[cn][j]
            jyear=dfb['Year'][j]
            if(iname==jname and y==jyear):
                flag=1
                co[i]=dfb['count'][j]
        if(flag==0):
            co[i]=0
    return co


# Function that return unique element list
def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]


# Function that returns dataframe of only presented students in exam
def present_students(all_students):
    
    all_examinee = all_students.loc[(all_students['Attendance'] == 'O') | (all_students['Attendance'] == '응시')]
    all_examinee = all_examinee.reset_index(drop=True)

    return all_examinee


# Function that count student no. of 1st to 4th year
def dfcol_count(df1,df2,colg1,colg2,cn):

    df1['Number of Applicant'] = " "
    df1['Number of Applicant'] = df1.groupby(colg1)[cn].transform('count')
    df1 = df1.drop_duplicates()

    df2['count'] = " "
    df2['count'] = df2.groupby(colg2)[cn].transform('count')

    # Droping previous index column and assign new index
    df1 = df1.reset_index()
    df1 = df1.drop(['index'],axis=1)

    # Creating new columns 
    df1['Year 1'] = yearcount(df1,df2,cn,1)
    df1['Year 2'] = yearcount(df1,df2,cn,2)
    df1['Year 3'] = yearcount(df1,df2,cn,3)
    df1['Year 4'] = yearcount(df1,df2,cn,4)

    return df1


def subtotal(afes_output):
    b = afes_output.groupby('Exam Region').count()>1
    t = b.loc[b['Exam Site'] == True]
    f = b.loc[b['Exam Site'] == False]
    tl=len(t)
    fl=len(f)
    # Appending subtotal in between
    temp = afes_output
    blank = []
    for i in range(0,tl):
        temp = afes_output.loc[afes_output['Exam Region']==t.index[i]].copy()
        temp.loc['Sub Total',2:] = temp.sum()
        blank.append(temp)
    for i in range(0,fl):
        temp = afes_output.loc[afes_output['Exam Region']==f.index[i]].copy()
        blank.append(temp)
    final_output = pd.concat(blank)
    if(tl>0):
        final_output.loc['Sub Total','Exam Site']='Sub Total'

    # Droping previous index column and assign new index
    final_output = final_output.reset_index()
    final_output = final_output.drop(['index'],axis=1)

    return final_output


# Formatting the style of each table and writing dataframe as the table to excel file #
def formatter(df, writer, sheet_name, startrow, startcol, heading='', index=True, total_row=False, mindex=False, header = False, noneng = False):

    sheetname = translation(sheet_name,noneng)
    init_row = startrow
    init_col = startcol
    if(heading != ''):
        startrow += 2
    # Translation columns header name for both single and multiindex column
    if (header == True):
        colname = []
        if isinstance(df.columns, pd.core.index.MultiIndex):
            levels = df.columns.levels
            for x in levels:
                y = []
                for j in x:
                    y.append(translation(j,noneng))
                colname.append(y)
            df.columns = df.columns.set_levels(colname)
        else:
            for x in df.columns:
                colname.append(translation(x,noneng))
            df.columns = colname
        row = startrow
    if (header == False):
        row = startrow + 1

    # Writing dataframe to excel sheet except the header row
    df.to_excel(writer, sheet_name = sheetname, startrow = row , startcol = startcol, header = header, index = index)
    # Calculating end row and end columns of the dataframe
    endcol = startcol + len(df.columns)
    endrow = startrow + len(df)

    # Get the xlsxwriter workbook and worksheet objects.
    workbook  = writer.book
    worksheet = writer.sheets[sheetname]

    # Setting row and columns according to dataframe format
    if (index==False):
        endcol = endcol - 1
    if (mindex==True):
        startcol = startcol + 1
        endcol = endcol + 1
    if (isinstance(df.columns, pd.core.index.MultiIndex)):
        endrow = endrow + 1
        if (header == True):
            endrow = endrow + 1
            startrow = startrow + 4
        
    # Adding a textbox
    if (heading != ''):
        text = translation(heading,noneng)
        text = '▣ ' + text
        options = {
            'height': 28,
            'width' : 400,
            'font': {'bold': True,'name': 'Arial','size': 12},
        }
        worksheet.insert_textbox(init_row, init_col, text, options)

    # Defining a header format.
    header_format = workbook.add_format({
        'bold': True,
        'align': 'center',
        'fg_color': '#f2f2f2',
        'border': 2 })
    if(sheet_name == 'Standing in detail' or sheet_name == 'Score Table'):
        merge_format = workbook.add_format({'align': 'center','bold':'True'})
        worksheet.merge_range('A2:H2',None) 
        worksheet.merge_range('A4:H4',translation('Correct Answers',noneng),merge_format)
    # Adding dataframe as excel table and styling it
    worksheet.add_table(startrow ,startcol, endrow, endcol, {
        'header_row': False,
        'banded_rows': False,
        'style': 'Table Style Light 15',
        'total_row': total_row })
    
    # Scaling Korean charecters width with english characters
    korl = 1
    if (noneng == True):
        korl = 1.6
    
    # Setting width of index column and writing index name
    if (index==True):
        if (mindex==False):
            series = pd.Series(df.index, dtype='str')
            if isinstance(df.columns, pd.core.index.MultiIndex):
                dfindex1 = translation(df.columns.names[0],noneng)
                dfindex2 = translation(df.columns.names[1],noneng)
                lsn1 = len(str(dfindex1))
                lsn2 = len(str(dfindex2))
                if(header == False):
                    worksheet.write(startrow ,startcol , dfindex1 , header_format)
                    worksheet.write(startrow + 1 ,startcol , dfindex2 , header_format)
                else:
                    worksheet.write(startrow - 4 ,startcol , dfindex1 , header_format)
                    worksheet.write(startrow - 3 ,startcol , dfindex2 , header_format)
            else:
                dfindex = translation(df.index.name,noneng)
                lsn1 = len(str(dfindex))
                lsn2 = 0
                if(header == False):
                    worksheet.write(startrow ,startcol , dfindex , header_format)
            max_len = max((series.map(len).max() , lsn1 , lsn2)) + 1
            if(series.dtype==np.object):
                max_len = int(max_len * korl)
            worksheet.set_column(startrow , startrow , max_len)
        else:
            for col_num, value in enumerate(df.index.levels):
                series = pd.Series(df.index.levels[col_num], dtype='str')
                if isinstance(df.columns, pd.core.index.MultiIndex):
                    dfindexname = translation(df.index.names[col_num],noneng)
                    dfcolumnsname = translation(df.columns.names[col_num],noneng)
                    lsn1 = len(str(dfindexname))
                    lsn2 = len(str(dfcolumnsname))
                    if (header == False):
                        worksheet.write(startrow , col_num , dfindexname , header_format)
                        worksheet.write(startrow + 1 , col_num , dfcolumnsname , header_format)
                    else:
                        worksheet.write(startrow - 4 ,col_num , dfindexname , header_format)
                        worksheet.write(startrow - 3 ,col_num , dfcolumnsname , header_format)
                else:
                    dfindexname = translation(df.index.names[col_num],noneng)
                    lsn1 = len(str(dfindexname))
                    lsn2 = 0
                    if (header == False):
                        worksheet.write(startrow , col_num , dfindexname , header_format)
                    else:
                        worksheet.write(startrow - 4 ,col_num , dfindexname , header_format)
                max_len = max((series.map(len).max() , lsn1 , lsn2)) + 1
                if(series.dtype==np.object):
                    max_len = int(max_len * korl)
                worksheet.set_column(col_num , col_num , max_len)
    # Write the column headers with the defined format and auto width.
    if (index == True):
        startcol += 1
    for col_num, value in enumerate(df.columns.values):
        series = df.iloc[:,col_num]
        col = startcol + col_num
        # checking whether the dataframe is multiindex column or not
        if isinstance(df.columns, pd.core.index.MultiIndex):
            value0 = translation(value[0],noneng)
            value1 = translation(value[1],noneng)
            lsn0 = len(str(value0))
            lsn1 = len(str(value1))
            # Considering only 2nd level index width in multindex column
            if(header == True):
                if(lsn1!=0):
                    lsn0=lsn1
                    
            if (header == False):
                worksheet.write(startrow , col , value0 , header_format)
                worksheet.write(startrow + 1 , col , value1 , header_format)
            else:
                worksheet.write(startrow - 4 , col , value0 , header_format)
                worksheet.write(startrow - 3 , col , value1 , header_format)
        else:
            value = translation(value,noneng)
            lsn1 = 0
            lsn0 = len(str(value))
            if (header == False):
                worksheet.write(startrow , col , value , header_format)
        # Setting columns width on the basic of its content length
        max_len = max((series.astype(str).map(len).max() , lsn0 , lsn1)) + 1
        if(series.dtype==np.object):
            max_len = int(max_len * korl)
        worksheet.set_column(col , col , max_len)
        if(sheet_name == 'Standing in detail'):
            lencol = len(df.columns)
            i = 10
            format1 = workbook.add_format({'num_format': '0.00'}) 
            while(i <= lencol):
                worksheet.set_column(i , i , None , format1) 
                i += 6
        if(sheet_name == 'Question Analysis'):
            format2 = workbook.add_format({'num_format': '0.0000'}) 
            for j in range(21,27):
                worksheet.set_row(j , None , format2)


def get_tuples(df, level=2):
    r=[]
    tup_type = type((1,2))
    for a in df.columns:
      if type(a) != tup_type:
        if level == 3:
            r.append((a,' ', ' '))
        else:
            r.append((a,''),)
      else:
        r.append(a)
    return r

def top_standing(df,year=0):
    score = df.iloc[:,9:10].copy()
    ts = df.iloc[:,0:5].copy()
    ts = ts.join(score)
    ts.columns = ts.columns.droplevel(1)
    ts = ts.rename(columns = {'Total':'Score'})
    if(year!=0):
        maxs = df.loc[df['Year']==year]['Total']['Scored Marks'].max()
        ts = ts.loc[(ts['Score']==maxs) & (ts['Year']==year)].copy()
        ts.insert(0, ('Grade') , ts.Year)
    return ts


#Translating text using translate.xlsx file as reference
def translation(original_text,noneng):
    if noneng:
        for target in trans:
            if(original_text==target):
                original_text = trans[target]
    return original_text


# Checking whether given string is all english or not
def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True

# For displaying colored output
def cprint(msg, foreground = "black", background = "white"):
    fground = foreground.upper()
    bground = background.upper()
    style = getattr(Fore, fground) + getattr(Back, bground)
    print(style + msg + Style.RESET_ALL)
