import input_class as ic
import functions as fn
import file_reader as fr
import pandas as pd
import numpy as np
import os


#Output folder and file name
output_foldername = fn.translation("Output/Output for Exam Analysis",fr.noneng)
output_filename = fn.translation('Exam_Analysis.xlsx',fr.noneng)
if not os.path.exists(output_foldername):
    os.makedirs(output_foldername)
output_filepath = output_foldername + '/' + output_filename
writer = pd.ExcelWriter(output_filepath)


#Assigning variable of file_reader(fr)
df = fr.df
df_student = fr.df_student
df_answers = fr.df_answers
correct = fr.correct
sub = fr.sub
ns = fr.ns
nsub = fr.nsub
qn = fr.qn
subject = fr.subject
question = fr.question
ng = fr.noneng


#### Applicant Summary Sheet Output ####
output_sheet_name = 'Applicant State'
msg = fn.translation("Generating Applicant State Sheet",ng)
print(msg)

## 1. Creating "Final Examinee List" output ##
# All students in exam List will be returned
fel_output = df_student
# Droping the Attendance Column
fel_output = fel_output.drop(['Attendance'], axis=1)
# Outputing the final dataframe to excel file
startrow = 0
startcol = 0
fel_output = fel_output.sort_values(by='Name')
fel_output.index.name = fn.translation('S.N',ng)
fn.formatter(fel_output, writer, output_sheet_name , startrow, startcol, heading = 'Final Examinee List(Except for Absentee)', index = False, noneng = ng)
startcol += len(fel_output.columns) + 2

## 2. Creating "Applicant for each Exam Site" Output ##
# Passing Arguments to the function which sum the numbers of students as required
# (By each Exam Site and also by each year with Exam Region included)
total = fn.translation('Total',ng)
person = fn.translation('Person',ng)
df1 = df_student.loc[:,'Exam Region':'Exam Site'].copy()
df2 = df_student.loc[:,'Year':'Exam Site'].copy()
colg1 = ['Exam Region','Exam Site']
colg2 = ['Exam Site','Year']
cn = 'Exam Site'
afes_output = fn.dfcol_count(df1,df2,colg1,colg2,cn)
# Calculate the new row total
afes_output.loc[total,2:] = afes_output.sum()
# Calculation of sub-total
# Passing the dataframe to insert subtotal in between the Exam Site
final_output = fn.subtotal(afes_output)
# Assigning the claculated total row at the end of the table
final_output.loc[total] = afes_output.loc[total]
final_output.loc[total,'Exam Region']=total
# Merging rows in Exam Region Column
final_output.loc[final_output['Exam Region'].duplicated(), 'Exam Region'] = np.NaN
# Adding Persons to the numbers
final_output.iloc[:,2:] = final_output.iloc[:,2:].astype(int)
final_output.iloc[:,2:] = final_output.iloc[:,2:].astype(str)
final_output.iloc[:,2:] += ' ' + person
# Outputing the final dataframe to excel file
fn.formatter(final_output, writer, output_sheet_name , startrow, startcol, heading = 'Applicant for each exam site', index = False, total_row = True, noneng = ng)
startrow = len(final_output) + 4

## 3. Creating "Applicant for each Institute" Output ##
# Passing Arguments to the function which sum the numbers of students as required
# (By each Institute and also by each year)
df1 = df_student.loc[:,'Institute':'Institute'].copy()
df2 = df_student.loc[:,'Institute':'Year'].copy()
colg1 = ['Institute']
colg2 = ['Institute','Year']
cn = 'Institute'
afi_output = fn.dfcol_count(df1,df2,colg1,colg2,cn)
afi_output.index = afi_output.index + 1
afi_output.loc[total,1:] = afi_output.sum()
afi_output.loc[total,'Institute']=total
# Adding Persons to the numbers
afi_output.iloc[:,1:] = afi_output.iloc[:,1:].astype(int)
afi_output.iloc[:,1:] = afi_output.iloc[:,1:].astype(str)
afi_output.iloc[:,1:] += ' ' + person

afi_output = afi_output.sort_values(by='Institute')
# Outputing the final dataframe to excel file
fn.formatter(afi_output, writer, output_sheet_name , startrow , startcol, heading = 'Applicant for each Institute', index = False, total_row = True, noneng = ng)
startcol += len(afi_output.columns) + 2


## 4. Creating "Attendance for each Exam Site" Output ##

startrow = 0
# Only presented students in exam List will be returned
present = fn.present_students(df_student)
# Passing Arguments to the function which sum the numbers of students as required
# (By each Exam Site and also by each year with Exam Region included)
df1 = present.loc[:,'Exam Region':'Exam Site'].copy()
df2 = present.loc[:,'Year':'Exam Site'].copy()
colg1 = ['Exam Region','Exam Site']
colg2 = ['Exam Site','Year']
cn = 'Exam Site'
atfes_output = fn.dfcol_count(df1,df2,colg1,colg2,cn)
# Calculate the new row total
atfes_output.loc[total,2:] = atfes_output.sum()
# Calculation of sub-total
# Passing the dataframe to insert subtotal in between the Exam Site
final_output = fn.subtotal(atfes_output)
# Assigning the claculated total row at the end of the table
final_output.loc[total] = atfes_output.loc[total]
final_output.loc[total,'Exam Region']=total
# Merging rows in Exam Region Column
final_output.loc[final_output['Exam Region'].duplicated(), 'Exam Region'] = np.NaN
# Adding Persons to the numbers
final_output.iloc[:,2:] = final_output.iloc[:,2:].astype(int)
final_output.iloc[:,2:] = final_output.iloc[:,2:].astype(str)
final_output.iloc[:,2:] += ' ' + person
# Outputing the final dataframe to excel file
fn.formatter(final_output, writer, output_sheet_name , startrow , startcol, heading = 'Attendance for each exam site', index = False, total_row = True, noneng = ng)
startrow = len(final_output) + 4

## 5. Creating "Attendance for each Institute" Output ##
# Only presented students in exam List will be returned
present = fn.present_students(df_student)
# Passing Arguments to the function which sum the numbers of students as required
# (By each Institute and also by each year)
df1 = present.loc[:,'Institute':'Institute'].copy()
df2 = present.loc[:,'Institute':'Year'].copy()
colg1 = ['Institute']
colg2 = ['Institute','Year']
cn = 'Institute'
atfi_output = fn.dfcol_count(df1,df2,colg1,colg2,cn)
atfi_output.index = atfi_output.index + 1
atfi_output.loc[total,1:] = atfi_output.sum()
atfi_output.loc[total,'Institute']=total
# Adding Persons to the numbers
atfi_output.iloc[:,1:] = atfi_output.iloc[:,1:].astype(int)
atfi_output.iloc[:,1:] = atfi_output.iloc[:,1:].astype(str)
atfi_output.iloc[:,1:] += ' ' + person

atfi_output = atfi_output.sort_values(by='Institute')
# Outputing the final dataframe to excel file
fn.formatter(atfi_output, writer, output_sheet_name , startrow , startcol , heading = 'Attendance for each Institute', index = False, total_row = True, noneng = ng)
startcol += len(atfi_output.columns) + 2

## 6. Creating output for "Question List" ##
startrow = 0
# Appending array of objects to list then converting it into dataframe
list = []
for i in range(0,nsub):
    list.append(vars(subject[i]))
op = pd.DataFrame(list,  columns=['name','startq','endq','tqno'], index=range(0,nsub))
op.columns = ['Category','Start Q. No.','End Q. No.','Number of Q.']
# Adding new column Notation in the final result
list = []
for i in range(1,nsub+1):
    list.append('Sub'+ str(i))
op['Notation in the Final result'] = list
# Calculate the new row totalsum
op.loc[total,'Number of Q.'] = op['Number of Q.'].sum()
op.loc[total,'Category']=total
# Outputing the final dataframe to excel file
fn.formatter(op, writer, output_sheet_name , startrow , startcol, heading = 'Question List', index = False, noneng = ng)
startcol += len(op.columns) + 2




#### Score Table Sheet Output ####
output_sheet_name = 'Score Table'
msg = fn.translation("Generating Score Table Sheet",ng)
print(msg)

## Creating output for "Score table" ##
# Comparing student answers with correct answers
cdf = (correct.values == df_answers)
# Computing columns in cdf
cdf.insert(0, 'Total Score', cdf.sum(axis=1), allow_duplicates=False)
cdf.insert(1, 'Standing', cdf['Total Score'].rank(method = 'min', ascending = False), allow_duplicates=False)
cdf.insert(2, 'Percentage', round(cdf['Standing']/len(present),4), allow_duplicates=False)
# Merging correct answer row with compared df(cdf)
cf = correct.copy()
cf.insert(0, 'Total Score', np.NaN)
cf.insert(1, 'Standing', np.NaN)
cf.insert(2, 'Percentage',np.NaN)
cf = cf.append(cdf)
cf.columns = cf.columns.droplevel([2,3])
# Resetting index
cf = cf.reset_index(drop=True)
# Making temp Studentdf
tsdf = df_student.copy()
st = tsdf.loc[0:0]
st = st.append(tsdf)
# Resetting index
st = st.reset_index(drop=True)
st.iloc[0] = " "
st.iloc[0,0] = fn.translation("Correct Answers",ng)
# converting the columns to multiindex
st.columns = [st.columns,['','','','','','','','','']]
# Merging two df students and computed answers
st = st.join(cf) 
# Multiindex tuples
f = fn.get_tuples(st)
st.columns = pd.MultiIndex.from_tuples(f)
ft = st.set_index('Examinee No.')
# Grouping columns with same 1st label index
ft = ft[ft.columns.levels[0]]
# Translation of column header
levels = ft.columns.levels
f = []
for x in levels:
    y = []
    for j in x:
        y.append(fn.translation(j,ng))
    f.append(y)
ft.columns = ft.columns.set_levels(f)
del ft.index.name
ft.columns = ft.columns.set_names([fn.translation('Examinee No.',ng),''])
# Passing scoretable df for foramtting and writing to excel file
fn.formatter(ft, writer , output_sheet_name , 0 , 0 , header= True ,  noneng = ng )




#### Standing in detail Sheet Output ####
output_sheet_name = 'Standing in detail'
msg = fn.translation("Generating Standing in detail Sheet",ng)
print(msg)

tsdf = df_student.iloc[:,:-1].copy()
full_marks = 100
i=0
subjects_tuple = []
for one in sub:
    tsdf[one,'Full Marks'] = subject[i].tqno
#     tsdf[one,'Converted Full Marks(up to 100)'] = full_marks
    tsdf[one,'Scored Marks'] = cdf[one].sum(axis=1)
    tsdf[one,'Converted Marks(Interms of 100)'] = round((tsdf[one,'Scored Marks']/tsdf[one,'Full Marks'])* full_marks, 2)
    tsdf[one,'Rank in total'] = tsdf[one,'Scored Marks'].rank(method = 'min', ascending = False)
    tsdf[one,'Rank in Year'] = tsdf.filter([(one,'Scored Marks'),'Year']).groupby(['Year']).rank(method = 'min', ascending=False)
    tsdf[one,'Rank in Institute'] = tsdf.filter([(one,'Scored Marks'),'Institute']).groupby(['Institute']).rank(method = 'min', ascending=False)
    subjects_tuple.append((one,'Scored Marks'))
    i += 1
r = fn.get_tuples(tsdf)
s = pd.DataFrame(np.transpose(tsdf.values), index = pd.MultiIndex.from_tuples(r))
tsdf = s.T.copy()
tsdf.insert(8, ('Total','Full Marks') , qn)
# tsdf.insert(9, ('Total','Converted Full Marks(up to 100)'), full_marks)
tsdf.insert(9, ('Total','Scored Marks'), tsdf.filter(subjects_tuple).sum(axis=1))
tsdf.insert(10, ('Total','Converted Marks(Interms of 100)'), round((tsdf['Total','Scored Marks']/tsdf['Total','Full Marks'])* full_marks,2))
tsdf.insert(11, ('Total','Rank in total'),tsdf['Total','Scored Marks'].rank(method = 'min', ascending = False))
tsdf.insert(12, ('Total','Rank in Year'),tsdf[[('Year',''),('Total','Scored Marks')]].groupby([('Year','')]).rank(method = 'min', ascending=False))
tsdf.insert(13, ('Total','Rank in Institute'),tsdf[[('Institute',''),('Total','Scored Marks')]].groupby([('Institute','')]).rank(method = 'min', ascending=False))
# tsdf[('Total','Converted Marks(Interms of 100)')] = round(tsdf('Total','Converted Marks(Interms of 100)'),2)
# Starting index from 1
tsdf.index = tsdf.index + 1
ft = tsdf.set_index('Examinee No.')
# Translation of column header
levels = ft.columns.levels
f = []
for x in levels:
    y = []
    for j in x:
        y.append(fn.translation(j,ng))
    f.append(y)
ft.columns = ft.columns.set_levels(f)
# Removing row index name and seting columns index name
del ft.index.name
ft.columns = ft.columns.set_names([fn.translation('Examinee No.',ng),''])

fn.formatter(ft, writer , output_sheet_name , 0 , 0 , header= True , noneng = ng )



#### Question Analysis Sheet Output ####
output_sheet_name = 'Question Analysis'
msg = fn.translation("Generating Question Analysis Sheet",ng)
print(msg)

## 1. Question in detail Output ##
m = correct.T
dra = fn.translation('Discrimination Rate(Ave:',ng)
cra = fn.translation('Correct Answer Rate(Ave:',ng)
# Only considering the students who have appeared in exam
cdf = cdf[cdf['Total Score'] != 0]
cdf.reset_index(drop=True)
m['Level of Difficulty','Winners'] = cdf.iloc[:,3:].sum()
m['Level of Difficulty','Total Examinees'] = len(cdf)
temp = m['Level of Difficulty','Winners']/m['Level of Difficulty','Total Examinees']
avg= round(temp.sum()/len(temp),4)
m['Level of Difficulty',cra + str(avg) + ')'] = round(temp,4)
m['Answer Discrimination Rate','Winners in Top 27%'] = ((cdf['Percentage'] <= .27) & cdf.iloc[:,3:].T).sum(axis=1)
m['Answer Discrimination Rate','Examinees in Top 27%'] = (cdf['Percentage'] <= .27).sum()
m['Answer Discrimination Rate','Winners in Low 27%'] = ((cdf['Percentage'] >= .73) & cdf.iloc[:,3:].T).sum(axis=1)
m['Answer Discrimination Rate','Examinees in Low 27%'] = (cdf['Percentage'] >= .73).sum()
temp = m['Answer Discrimination Rate','Winners in Top 27%']/m['Answer Discrimination Rate','Examinees in Top 27%'] - m['Answer Discrimination Rate','Winners in Low 27%']/m['Answer Discrimination Rate','Examinees in Low 27%']
avg= round(temp.sum()/len(temp),4)
m['Answer Discrimination Rate',dra + str(avg) + ')'] = round(temp,4)
# Obtaining only examinee answers list
present = df_answers.copy()
present = present[present.sum(axis=1) != 0]
total = 0 
for i in [1,2,3,4,5,'None']:
    m['Number of Respondent',i] = (present == i).sum()
    total += m['Number of Respondent',i]
m['Number of Respondent','Total'] = total
total = 0 
for i in [1,2,3,4,5,'None']:
    m['Answer Response Rate', i] = round((present == i).sum()/len(cdf),4)
    total += m['Answer Response Rate',i]
m['Answer Response Rate','Total'] = round(total,2)
m.rename(columns={0:('Level of Difficulty','Correct Answers')}, inplace=True)
r=[]
tup_type = type((1,2))
for a in m.columns:
    if a == '':
        r.append((' ', ' '))
    elif type(a) != tup_type:
        r.append((a,' '),)
    else:
        r.append(a)
m = pd.DataFrame(np.transpose(m.values), index = pd.MultiIndex.from_tuples(r)).copy()
m.columns = st.iloc[:,12:].columns
m.index.names = ([fn.translation('Category',ng) , fn.translation('Subjects',ng)])
m.columns.names = (['' , fn.translation('Question No.',ng)])
startrow = 0
# Grouping columns with same 1st label index
m = m[m.columns.levels[0]]

# Passing Question Analysis df for foramtting and writing to excel file
fn.formatter(m, writer , output_sheet_name , startrow , 0 , heading = 'Question in detail' , mindex=True , header = True , noneng = ng )
startrow += len(m) + 6

## 2.Subject in detail Output ##
list = []
for i in range(0,nsub):
    list.append(vars(subject[i]))
op = pd.DataFrame(list,  columns=['name','startq','endq','tqno'], index=range(0,nsub))
op.columns = [('Category',''),('Question No.','Start Q. No.'),('Question No.','End Q. No.'),('Full Marks','')]
a = m.mean(axis=1, level=0)
a = round(a.iloc[[3,8],:].T,4)
a.columns = [('Average of the Level',''),('Average of Distinguishable Rate','')]
a = a.reset_index(drop=True)
op = op.join(a)
a = m.sum(axis=1,level=0)
a = round(a.iloc[[1],:].T/len(cdf),4)
a.columns = [('Average Score for each year','Total')]
a = a.reset_index(drop=True)
op = op.join(a)
for year in range(1,5):
    list1 = []
    for subj in sub:
        a = tsdf.loc[(tsdf['Year']==year) & (tsdf['Attendance']=='O')]
        s = a[subj]['Scored Marks'].sum()
        l = len(a)
        if(l <= 0):
            l = 1
        list1.append(round(s/l,4))
    op[('Average Score for each year',year)] = list1
list1 = []
list2 = []
a = tsdf.loc[tsdf['Attendance']=='O']
# Excluded 0 for calculating min value
a = a.replace(0,np.NaN)
for subj in sub:
    maxs = a[subj]['Scored Marks'].max()
    mins = a[subj]['Scored Marks'].min(skipna=True)
    list1.append(maxs)
    list2.append(mins)
op[('Maximum','')] = list1
op[('Minimum','')] = list2
# Calcutating total Mean
a = m.mean(axis=1)
op.loc['Total Mean',3:] = op.sum()
op.loc['Total Mean',('Category','')] = "Total Mean"
op.loc['Total Mean',('Average of the Level','')] = round(a.iloc[3],4)
op.loc['Total Mean',('Average of Distinguishable Rate','')] = round(a.iloc[8],4)
op.loc['Total Mean',('Maximum','')] = cdf['Total Score'].max()
op.loc['Total Mean',('Minimum','')] = cdf['Total Score'].min()
if ng:
    op.loc['Total Mean',('Category','')] = fn.translation("Total Mean",fn.trans)
i=6
for col in op.iloc[:,6:]:
    ncol = (col[0] + '(Converted Score)',col[1])
    op[ncol] = round((op.iloc[:,i]*100)/op.iloc[:,3],4)
    i += 1
    op.columns = pd.MultiIndex.from_tuples(fn.get_tuples(df = op, level =2))
op = op.rename(columns = {'Maximum(Converted Score)':'Max'})
op = op.rename(columns = {'Minimum(Converted Score)':'Min'})
op = op.set_index('Category')
op.index.name = fn.translation('Category',ng)
op[('Max','')] = round(op[('Max','')],0)
op[('Min','')] = round(op[('Min','')],0)
op.columns = op.columns.set_names([fn.translation('Category',ng),fn.translation('Subjects',ng)])
# Passing Question Analysis df for foramtting and writing to excel file
fn.formatter(op, writer , output_sheet_name , startrow , 0 , heading = 'Subject in detail' , header = True , noneng = ng )
# op.to_excel(writer, sheet_name = output_sheet_name , startrow = startrow + 1)


## Plotting charts in the sheet "Question Analysis" ## 

output_sheet_name = fn.translation(output_sheet_name,ng)
workbook  = writer.book
worksheet = writer.sheets[output_sheet_name]
erow = startrow + len(op)
srow = erow - len(op) + 4
## 1. Level & discrimination for each subject Bar Chart ##
chart1 = workbook.add_chart({'type': 'column'})
name = fn.translation('Average of the Level',ng)
chart1.add_series({
    'name': name,
    'categories': [output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,4,erow,4],
    'line':       {'color': 'blue'},
})
name = fn.translation('Average of Distinguishable Rate',ng)
chart1.add_series({
    'name': name,
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,5,erow,5],
    'line':       {'color': 'green'},
})
name = fn.translation('Level & discrimination for each subject',ng)
chart1.set_title ({'name': name, 'name_font': {'size': 12, 'bold': True}})
chart1.set_legend({'position': 'bottom'})
chart1.set_size({'x_scale': 1.2, 'y_scale': 1.5})
# Insert the chart into the worksheet.
worksheet.insert_chart(erow+6,0,chart1)

## 2. Average Score for each school years (Origin Score) Bar Chart ##
chart2 = workbook.add_chart({'type': 'column'})
name = fn.translation('Full Marks',ng)
chart2.add_series({
    'name': name,
    'categories': [output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,3,erow,3],
})
name = 'Total'
if ng:
    name = fn.translation(name,fn.trans)
chart2.add_series({
    'name': name,
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,6,erow,6],
})
chart2.add_series({
    'name': '1',
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,7,erow,7],
})
chart2.add_series({
    'name': '2',
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,8,erow,8],
})
chart2.add_series({
    'name': '3',
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,9,erow,9],
})
chart2.add_series({
    'name': '4',
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,10,erow,10],
})
name = fn.translation('Average Score for each years (Original Score)',ng)
chart2.set_title ({'name': name, 'name_font': {'size': 12, 'bold': True}})
chart2.set_legend({'position': 'bottom'})
chart2.set_size({'x_scale': 1.2, 'y_scale': 1.5})
line_chart1 = workbook.add_chart({'type': 'line'})
# Configure the data series for the secondary chart.
name = fn.translation('Max',ng)
line_chart1.add_series({
    'name':       name,
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,11,erow,11],
})
name = fn.translation('Min',ng)
line_chart1.add_series({
    'name':       name,
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,12,erow,12],
})
# Combine the charts.
chart2.combine(line_chart1)
# Insert the chart into the worksheet.
worksheet.insert_chart(erow+6,6,chart2)

## 3. Average Score for each school years (Converted Score) Bar Chart ##
chart3 = workbook.add_chart({'type': 'column'})
name = fn.translation('Total',ng)
chart3.add_series({
    'name': name,
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,13,erow,13],
})
chart3.add_series({
    'name': '1',
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,14,erow,14],
})
chart3.add_series({
    'name': '2',
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,15,erow,15],
})
chart3.add_series({
    'name': '3',
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,16,erow,16],
})
chart3.add_series({
    'name': '4',
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,17,erow,17],
})
name = fn.translation('Average Score for each years (Converted Score)',ng)
chart3.set_title ({'name': name, 'name_font': {'size': 12, 'bold': True}})
chart3.set_legend({'position': 'bottom'})
chart3.set_size({'x_scale': 1.2, 'y_scale': 1.5})
line_chart2 = workbook.add_chart({'type': 'line'})
# Configure the data series for the secondary chart.
name = fn.translation('Max',ng)
line_chart2.add_series({
    'name':       name,
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,18,erow,18],
})
name = fn.translation('Min',ng)
line_chart2.add_series({
    'name':       name,
    'categories':[output_sheet_name,srow,0,erow,0],
    'values':   [output_sheet_name,srow,19,erow,19],
})
# Combine the charts.
chart3.combine(line_chart2)
# Insert the chart into the worksheet.
worksheet.insert_chart(erow+6,15,chart3)




#### Summary Sheet Output ####
output_sheet_name = 'Summary'
msg = fn.translation("Generating Summary Sheet",ng)
print(msg)


total = fn.translation('Total',ng)
## 1. Attendance state Output ##
present_df = df_student.copy()
df1 = present_df.loc[:,'Exam Region':'Exam Site'].copy()
present_df = present_df.loc[present_df['Attendance'] == 'O']
df2 = present_df.loc[:,'Exam Region':'Exam Site'].copy()
df1['Applicant'] = df1.groupby(['Exam Region','Exam Site'])['Exam Site'].transform('count')
df1['Attendance'] = df2.groupby(['Exam Region','Exam Site'])['Exam Site'].transform('count')
df1 = df1.drop_duplicates()
df1 = df1.dropna()
df1['Absent'] = df1['Applicant']-df1['Attendance']
df1 = df1.set_index(['Exam Region','Exam Site'])
tsdf = tsdf.replace(0,np.NaN)
df1['Average'] = pd.pivot_table(tsdf, values='Total', index=['Exam Region', 'Exam Site'], aggfunc=np.mean)['Scored Marks']
df1['Max'] = pd.pivot_table(tsdf, values='Total', index=['Exam Region', 'Exam Site'], aggfunc=np.max)['Scored Marks']
df1['Min'] = pd.pivot_table(tsdf, values='Total', index=['Exam Region', 'Exam Site'], aggfunc=np.min).dropna()['Scored Marks']
#calculating Total row
totalm = df1['Average'].mean()
maxi = df1['Max'].max()
mini = df1['Min'].min()
df1.loc[(total,'-'),:] = df1.sum()
df1.loc[(total,'-'),'Average'] = totalm
df1.loc[(total,'-'),'Max'] = maxi
df1.loc[(total,'-'),'Min'] = mini
df1['Absentee'] = ' - '
df1['Issue'] = ' - '
df1['Average'] = round(df1['Average'],2)
startrow = 0
fn.formatter(df1, writer , output_sheet_name , startrow , 0 , heading = 'Attendance state' , mindex=True , noneng = ng)
startrow = len(df1) + 4
## 2. The top standings ##
ts = fn.top_standing(tsdf)
ts = ts.sort_values('Score', ascending=False).head(6)
ts = ts.reset_index(drop=True)
ts.index = ts.index + 1
ts.index.name = "Standing"
ts = ts.reset_index()
i = 0
temp = 0
for score in ts['Score']:
    if(temp==score):
       ts.loc[i,'Standing'] = ts.loc[i-1,'Standing']
    temp = score
    i += 1
ts = ts.set_index('Standing')
fn.formatter(ts, writer , output_sheet_name , startrow , 0 , heading = 'Top Standings' , noneng = ng)
startrow += len(ts) + 4



## 3. The top standings(Year-Wise) ##

ts1 = fn.top_standing(tsdf,1)
ts2 = fn.top_standing(tsdf,2)
ts3 = fn.top_standing(tsdf,3)
ts4 = fn.top_standing(tsdf,4)
tsy = pd.concat([ts1,ts2,ts3,ts4])
tsy = tsy.set_index('Grade',drop=True)
fn.formatter(tsy, writer , output_sheet_name , startrow , 0 , heading = 'Top Standings in each year' , noneng = ng)
startrow += len(tsy) + 4


## 4. Average Score for each institute ##

present_df = df_student.copy()
present_df = present_df.loc[(present_df['Attendance'] == 'O')|(present_df['Attendance'] == '응시')]
df1 = present_df.loc[:,'Institute':'Institute'].copy()
df1 = df1.sort_values('Institute')
df1['Applicant'] = df1.groupby('Institute')['Institute'].transform('count')
df1 = df1.drop_duplicates()
df1 = df1.set_index('Institute',drop=True)
df1['Average Score'] = pd.pivot_table(tsdf, values='Total', index=['Institute'], aggfunc=np.mean)['Scored Marks']
totalm = df1['Average Score'].mean()
df1.loc['Total'] = df1.sum()
df1.loc['Total','Average Score'] = totalm
df1 = df1.reset_index()
df1.iat[-1,0] = total
df1 = df1.set_index('Institute',drop=True)


df1['Average Score'] = round(df1['Average Score'],2)
# df1 = df1.style.format({'Average Score': "{:.2f}"})
df1['Rank of Institute'] = df1['Average Score'].rank(method = 'min', ascending=False)
df1 = df1.sort_values(by='Rank of Institute')

fn.formatter(df1, writer , output_sheet_name , startrow , 0 , heading = 'Average Score for each institute' , noneng = ng)

if ng:
    fn.cprint(output_filename + " 결과 파일 출력됨", "green", "black")
else:
    fn.cprint("Output file " + output_filename + " Created", "green", "black")


writer.save()
writer.close()