import input_class as ic
import functions as fn
import pandas as pd

#### Parsing the input data from the excel file as needed ####

# Input excel-file name and excel-sheet name
input_filename = 'input.xlsx'
# input_sheetname = 'Answer Sheet(1st input)'
input_sheetname = '응답'

# Loading the input excel file into dataframe
df_all = pd.read_excel(input_filename, header=[0, 1, 2, 3], sheet_name=input_sheetname)

# Checking whether it is english or non-english language
if fn.isEnglish(df_all.columns[3][0]):
    noneng = False
else:
    noneng = True


# Renaming all columns header name so that language dosn't matter
# Creating a new column for index
df_all = df_all.rename_axis(['Examinee No.']).reset_index()
df_all.columns.names = ['Index', '', ' ', '  ']
column_name = ['Name','License No.','Institute','Year','Exam Region','Exam Site','Attendance']
for i in range(1,8):
    df_all = df_all.rename(columns={ df_all.columns[i][0] : column_name[i-1] })
df_all =df_all.rename(columns={ df_all.columns[8][0] : 'Category' , df_all.columns[8][1] : 'Question No.' , df_all.columns[8][2] : 'Q. Bank ID' , df_all.columns[8][3] : 'Q. Type'})

# Loading the input excel file into dataframe without correct answer row
df = df_all.iloc[1:]
# resetting index of df
df = df.reset_index(drop=True)


# Getting only correct answer row data
correct_all = df_all.iloc[0:1]
#cleaned correct answer df
correct = correct_all.iloc[:,9:]

# Copying the first 9 columns which only contains student info to new dataframe
df_student = df.iloc[:,0:9]
df_student.columns = ['Examinee No.','Name','License No.','Institute','Year','Exam Region','Exam Site','Attendance','Q. Type']
# Removing spaces in Year column values if existed to make it integer
df_student['Year'] = df_student['Year'].astype('int64')

# Copying all columns except first 9 columns to another dataframe
df_answers = df.iloc[:,9:]


# Headers of all columns of correct df
headers = correct.columns.values
# Retrieving only subjects,questions no,questions bank id,questions type
allsub = [x[0] for x in headers]
sub = fn.unique(allsub)
allqno = [x[1] for x in headers]
allqbid = [x[2] for x in headers]
allqt = [x[3] for x in headers]

# Number of students,subjects,questions
ns = len(df.values)
nsub = len(sub)
qn = len(allqno)

# Calculating the starting and ending questions for each subjects
l = len(headers)
sname = headers[0][0]
qstart = [] * nsub
qend = [] * nsub
qstart.append(headers[0][1])
for i in range(0,l):
    tname = headers[i][0]
    if(sname != tname):
        qend.append(headers[i-1][1])
        qstart.append(headers[i][1])
        sname = tname
qend.append(headers[i][1])


# Creating object for each subject with total questions number
subject = []
for i in range(0, nsub):
    name = sub[i]
    sq = qstart[i]
    eq = qend[i]
    tqno = allsub.count(sub[i])
    subject.append(ic.Subjects(name, sq, eq, tqno))

# Creating question object for each questions
question = []
for i in range(0, qn):
    qno = allqno[i]
    qbid = allqbid[i]
    qtype = allqt[i]
    cans = correct.iloc[0][i]
    question.append(ic.Questions(qno, qbid, qtype, cans))