import pandas as pd
import numpy as np
import xlsxwriter as xls
import file_reader as fr
import functions as fn
import os


correct = fr.correct
df_answers = fr.df_answers
df_student = fr.df_student
nsub = fr.nsub
ng = fr.noneng
subject = fr.subject

#Output folder and file name
output_foldername = fn.translation("Output/Output for Customers",ng)
output_filename = fn.translation('Subject Result for each Institute.xlsx',ng)
if not os.path.exists(output_foldername):
    os.makedirs(output_foldername)
output_filepath = output_foldername + '/' + output_filename
# writer = pd.ExcelWriter(output_filepath)
workbook  = xls.Workbook(output_filepath)

# Generates dataframe by checking individual answer to correct answers
cdf = (correct.values == df_answers)
# Return only present students
df_present = fn.present_students(df_student)
ayear = ['1st Year','2nd Year','3rd Year','4th Year','Total']
# Student info with marks of individual subject
temp = cdf.sum(axis=1,level=0)
sdf = df_student.join(temp)
sdf = fn.present_students(sdf)

#Calculation of standing of each Institute by average score in each subject yearwise
sdf_stand = sdf[['Institute','Year']].copy()
sdf_stand = sdf_stand.join(sdf.iloc[:,9:])
year_stand = sdf_stand.groupby(['Institute','Year']).mean()
total_stand = sdf_stand.groupby(['Institute']).mean()
year_stand = year_stand.groupby(['Year']).rank(method = 'min', ascending = False)
total_stand = total_stand.rank(method = 'min', ascending = False)

# Calculation of total row at the last of the table
sdf_total = df_student[['Institute','Year']].copy()
sdf_total.insert(2,'Score',cdf.sum(axis=1))
sdf_total = sdf_total.loc[sdf_total['Score']!=0]
total_year_avg = sdf_total.groupby(['Institute','Year']).mean()
total_final_avg = sdf_total[['Institute','Score']].groupby(['Institute']).mean()
total_year_stand = total_year_avg.groupby(['Year']).rank(method = 'min', ascending = False)
total_final_stand = total_final_avg.rank(method = 'min', ascending = False)

listins = df_student.Institute.unique()
for i in listins:
    # Name of the institute
    output_sheet_name = i  
    worksheet = workbook.add_worksheet(output_sheet_name)

    # Displaying the main header textbox
    row = 1
    col = 1
    text = fn.translation('Result for each Institute',ng)
    options = {
        'width': 1280,
        'height': 50,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 20},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#e6e6e6'},
        'border': {'color': 'black','width': 2},
    }
    worksheet.insert_textbox(row, col, text, options)
    # Displaying Institute Name
    row += 4
    col = 15
    text = fn.translation('Institute',ng)
    options = {
        'width': 128,
        'height': 30,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 14},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#e6e6e6'},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)
    text = i
    options = {
        'width': 256,
        'height': 30,
        'x_offset': 128,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 14},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)

    # Main Table begins
    row = row + 4
    col = 4
    text = fn.translation('Applicant (Attendance)',ng)
    options = {
        'width': 192,
        'height': 40,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 12},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#f5f5f5'},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)
    text = fn.translation('Subjects',ng)
    options = {
        'width': 128,
        'height': 40,
        'y_offset':40,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 12},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#f5f5f5'},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)
    text = fn.translation('Full Marks',ng)
    options = {
        'width': 64,
        'height': 40,
        'y_offset': 40,
        'x_offset': 128,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 12},
        'align': {'horizontal': 'center','vertical': 'middle'},
        'fill': {'color': '#f5f5f5'},
        'border': {'color': 'black','width': 1},
    }
    worksheet.insert_textbox(row, col, text, options)
    # Applicant(Attendance) in each year as well as total
    for j in range(1,6):
        if j<=4:
            applicant = len(df_student.loc[(df_student['Institute']==i) & (df_student['Year']==j)])
            attendance = len(df_present.loc[(df_present['Institute']==i) & (df_present['Year']==j)])
            avg = sdf.loc[(sdf['Institute']==i) & (sdf['Year']==j)]
            avg = avg.iloc[:,9:].mean()
            try:
                stand = year_stand.loc[i].loc[j]
            except:
                stand = 0
            try:
                total_std = total_year_stand.loc[i].loc[j]
                total_avg = total_year_avg.loc[i].loc[j]['Score']
            except:
                total_std = 0
                total_avg = 0.0
        else:
            applicant = len(df_student.loc[df_student['Institute']==i])
            attendance = len(df_present.loc[df_present['Institute']==i])
            avg = sdf.loc[sdf['Institute']==i]
            avg = avg.iloc[:,9:].mean()
            try:
                stand = total_stand.loc[i]
            except:
                stand = 0
            try:
                total_std = total_final_stand.loc[i]
                total_avg = total_final_avg.loc[i]['Score']
            except:
                total_std = 0
                total_avg = 0.0
        text = str(applicant) + '  ( ' + str(attendance) +' ) '
        avg = np.nan_to_num(avg, copy = True)
        options = {
            'width': 128,
            'height': 40,
            'x_offset' : 192 + (j-1)*128,
            'font': {'name': 'Arial','align': 'center','size': 10},
            'align': {'horizontal': 'center','vertical': 'middle'},
            'border': {'color': 'black','width': 1},
        }
        worksheet.insert_textbox(row, col, text, options)
        text = fn.translation(ayear[j-1],ng)
        options = {
            'width': 128,
            'height': 20,
            'y_offset':40,
            'x_offset': 192 + (j-1)*128,
            'font': {'bold': True,'name': 'Arial','align': 'center','size': 12},
            'align': {'horizontal': 'center','vertical': 'middle'},
            'fill': {'color': '#f5f5f5'},
            'border': {'color': 'black','width': 1},
        }
        worksheet.insert_textbox(row, col, text, options)
        text = fn.translation('Avg.',ng)
        options = {
            'width': 64,
            'height': 20,
            'y_offset':60,
            'x_offset': 192 + (j-1)*128,
            'font': {'bold': True,'name': 'Arial','align': 'center','size': 8},
            'align': {'horizontal': 'center','vertical': 'middle'},
            'fill': {'color': '#f5f5f5'},
            'border': {'color': 'black','width': 1},
        }
        worksheet.insert_textbox(row, col, text, options)
        text = fn.translation('Standing',ng)
        options = {
            'width': 64,
            'height': 20,
            'y_offset':60,
            'x_offset': 256 + (j-1)*128,
            'font': {'bold': True,'name': 'Arial','align': 'center','size': 10},
            'align': {'horizontal': 'center','vertical': 'middle'},
            'fill': {'color': '#f5f5f5'},
            'border': {'color': 'black','width': 1},
        }
        worksheet.insert_textbox(row, col, text, options)
        for k in range(0,nsub+1):
            if(k<nsub):
                sub_name = subject[k].name
                text = str(round(avg[k],2))
                options = {
                    'width': 64,
                    'height': 40,
                    'y_offset': 80 + k*40,
                    'x_offset' : 192 + (j-1)*128,
                    'font': {'name': 'Arial','align': 'center','size': 10},
                    'align': {'horizontal': 'center','vertical': 'middle'},
                    'border': {'color': 'black','width': 1},
                }
                worksheet.insert_textbox(row, col, text, options)
                try:
                    text = str(int(stand[sub_name]))
                except:
                    text = '-'
                options = {
                    'width': 64,
                    'height': 40,
                    'y_offset': 80 + k*40,
                    'x_offset' : 256 + (j-1)*128,
                    'font': {'name': 'Arial','align': 'center','size': 10},
                    'align': {'horizontal': 'center','vertical': 'middle'},
                    'border': {'color': 'black','width': 1},
                }
                worksheet.insert_textbox(row, col, text, options)
            # Displaying the total row at the last of the table
            else:
                text = str(round(total_avg,2))
                options = {
                    'width': 64,
                    'height': 40,
                    'y_offset': 82 + k*40,
                    'x_offset' : 192 + (j-1)*128,
                    'font': {'name': 'Arial','align': 'center','size': 10},
                    'align': {'horizontal': 'center','vertical': 'middle'},
                    'border': {'color': 'black','width': 1},
                }
                worksheet.insert_textbox(row, col, text, options)
                if(type(total_std)==int):
                    text = '-'
                else:
                    text = str(int(total_std))
                options = {
                    'width': 64,
                    'height': 78,
                    'y_offset': 82 + k*40,
                    'x_offset' : 256 + (j-1)*128,
                    'font': {'name': 'Arial','align': 'center','size': 10},
                    'align': {'horizontal': 'center','vertical': 'middle'},
                    'border': {'color': 'black','width': 1},
                }
                worksheet.insert_textbox(row, col, text, options)
                conv_avg = (total_avg * 100)/len(fr.allsub)
                text = str(round(conv_avg,2))
                options = {
                    'width': 64,
                    'height': 40,
                    'y_offset': 120 + k*40,
                    'x_offset' : 192 + (j-1)*128,
                    'font': {'name': 'Arial','align': 'center','size': 10},
                    'align': {'horizontal': 'center','vertical': 'middle'},
                    'border': {'color': 'black','width': 1},
                }
                worksheet.insert_textbox(row, col, text, options)
    # Subject wise calculation
    row += 4
    for m in range(0,nsub+1):
        if(m<nsub):
            sub_name = subject[m].name
            full_marks = subject[m].tqno
            text = fn.translation(sub_name,ng)
            options = {
                'width': 128,
                'height': 40,
                'y_offset': m*40,
                'font': {'bold': True,'name': 'Arial','align': 'center','size': 10},
                'align': {'horizontal': 'center','vertical': 'middle'},
                'fill': {'color': '#f5f5f5'},
                'border': {'color': 'black','width': 1},
            }
            worksheet.insert_textbox(row, col, text, options)
            text = str(full_marks)
            options = {
                'width': 64,
                'height': 40,
                'x_offset': 128,
                'y_offset': m*40,
                'font': {'bold': True,'name': 'Arial','align': 'center','size': 10},
                'align': {'horizontal': 'center','vertical': 'middle'},
                'border': {'color': 'black','width': 1},
            }
            worksheet.insert_textbox(row, col, text, options)
        # Last total row index
        else:
            text = fn.translation('Total Score (Converted Score)',ng)
            options = {
                'width': 128,
                'height': 78,
                'y_offset': 2 + m*40,
                'font': {'bold': True,'name': 'Arial','align': 'center','size': 10},
                'align': {'horizontal': 'center','vertical': 'middle'},
                'fill': {'color': '#f5f5f5'},
                'border': {'color': 'black','width': 1},
            }
            worksheet.insert_textbox(row, col, text, options)
            text = str(len(fr.allsub))
            options = {
                'width': 64,
                'height': 78,
                'x_offset': 128,
                'y_offset': 2 + m*40,
                'font': {'bold': True,'name': 'Arial','align': 'center','size': 10},
                'align': {'horizontal': 'center','vertical': 'middle'},
                'border': {'color': 'black','width': 1},
            }
            worksheet.insert_textbox(row, col, text, options)

    row  = row + (m*2 + 4) + 2
    text = fn.translation('Total Institute and Examinees (Include Absentee :  ',ng)
    text = '▣ ' + text + str(fr.ns-len(df_present)) + ' ' + fn.translation('Person',ng) + ' )'
    options = {
        'width': 512,
        'height': 30,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 12},
        'align': {'horizontal': 'center','vertical': 'middle'},
    }
    worksheet.insert_textbox(row, col, text, options)

    row += 2
    for j in range(1,6):
        if j<=4:
            institute = df_student.loc[df_student['Year']==j]
            ins_no = len(institute.Institute.unique())
            person_no = len(institute)
        else:
            ins_no = len(df_student.Institute.unique())
            person_no = len(df_student)
        text = fn.translation(ayear[j-1],ng)
        options = {
            'width': 192,
            'height': 30,
            'x_offset': (j-1)*192,
            'font': {'bold': True,'name': 'Arial','align': 'center','size': 12},
            'align': {'horizontal': 'center','vertical': 'middle'},
            'fill': {'color': '#e6e6e6'},
            'border': {'color': 'black','width': 1},
        }
        worksheet.insert_textbox(row, col, text, options)
        text1 = fn.translation('Institute',ng)
        text2 = fn.translation('Person',ng)
        text = str(ins_no) + '  ' + text1 + '\n' + str(person_no) + '  ' + text2
        options = {
            'width': 192,
            'height': 50,
            'y_offset': 30,
            'x_offset': (j-1)*192,
            'font': {'name': 'Arial','align': 'center','size': 12},
            'align': {'horizontal': 'center','vertical': 'middle'},
            'fill': {'color': '#f9f9f9'},
            'border': {'color': 'black','width': 1},
        }
        worksheet.insert_textbox(row, col, text, options)

    # Displaying Graphs
    row += 6
    text = fn.translation('Result Distribution Graph of All Examinees',ng)
    text = '▣ ' + text
    options = {
        'width': 448,
        'height': 30,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 12},
        'align': {'horizontal': 'center','vertical': 'middle'},
    }
    worksheet.insert_textbox(row, col, text, options)
    col = 13
    text = fn.translation('Result Distribution Graph of your Institute',ng)
    text = '▣ ' + text
    options = {
        'width': 448,
        'height': 30,
        'font': {'bold': True,'name': 'Arial','align': 'center','size': 12},
        'align': {'horizontal': 'center','vertical': 'middle'},
    }
    worksheet.insert_textbox(row, col, text, options)
workbook.close()